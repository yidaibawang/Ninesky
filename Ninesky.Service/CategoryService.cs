﻿using Microsoft.EntityFrameworkCore;
using Ninesky.Entities;
using Ninesky.InterfaceService;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.Service
{
    public class CategoryService : BaseService<Category>, InterfaceCategoryService
    {
        public CategoryService(NineskyDbContext nineskyDbContext) : base(nineskyDbContext) { }

        public override async Task<OperationResult> AddAsync(Category category)
        {
            OperationResult operationResult = new OperationResult();
            if (category == null)
            {
                operationResult.Succeed = false;
                operationResult.Message = "栏目数据为空";
            }
            else
            {
                await dbContext.Categories.AddAsync(category);
                if (await dbContext.SaveChangesAsync() > 0)
                {
                    operationResult.Succeed = true;
                    operationResult.Message = "添加栏目成功";
                }
                else
                {
                    operationResult.Succeed = false;
                    operationResult.Message = "保存数据失败";
                }
            }
            return operationResult;
        }

        public async Task<OperationResult> ChangeParentParthAsync(string originalParth, string newParth)
        {
            OperationResult operationResult = new OperationResult();
            if (dbContext.Categories.Any(c => c.ParentPath == originalParth))
            {
                var categories = dbContext.Categories.Where(c => c.ParentPath == originalParth);
                foreach (var category in categories)
                {
                    category.ParentPath = newParth;
                }
                try
                {
                    operationResult.Code = await dbContext.SaveChangesAsync();
                    operationResult.Succeed = true;
                    operationResult.Message = "成功修改了"+operationResult.Code+"条记录。";
                }
                catch
                {
                    operationResult.Succeed = false;
                    operationResult.Message = "保存记录时发生错误。";
                }
                
            }
            else
            {
                operationResult.Succeed = true;
                operationResult.Code = 0;
                operationResult.Message = "无需要修改的记录";
            }
            return operationResult;
        }

        public IQueryable<Category> FindChildren(int id)
        {
            return dbContext.Set<Category>().AsNoTracking().Where(c => c.ParentId == id).OrderBy(c => c.Order);
        }
        public IQueryable<Category> FindListByContentType(ContentType contentType, bool incParent = false)
        {
            var categories = dbContext.Set<Category>().Where(c => c.ContentType == contentType);
            if (incParent && categories != null)
            {
                List<int> categoryIds = categories.Select(c => c.CategoryId).ToList();
                List<string> categoryParents = categories.Select(c => c.ParentPath).ToList();
                foreach(var parent in categoryParents)
                {
                    var idArryString = parent.Split(new char[] { ',' });
                    foreach(var idString in idArryString)
                    {
                        categoryIds.Add(int.Parse(idString));
                    }
                }
                categories = dbContext.Set<Category>().Where(c=>categoryIds.Contains(c.CategoryId));
            }
            return categories;
        }

        public async Task<string> FindNameAsync(int id)
        {
            if (dbContext.Set<Category>().Any(c => c.CategoryId == id)) return (await dbContext.Set<Category>().FindAsync(id)).Name;
            else return string.Empty;
            
        }

    }
}
