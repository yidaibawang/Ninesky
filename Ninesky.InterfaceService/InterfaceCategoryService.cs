﻿using Ninesky.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Ninesky.InterfaceService
{
    public interface InterfaceCategoryService:InterfaceBaseService<Category>
    {
        /// <summary>
        /// 修改父栏目路径
        /// </summary>
        /// <param name="originalParth">原路径</param>
        /// <param name="newParth">新路径</param>
        /// <returns></returns>
        Task<OperationResult> ChangeParentParthAsync(string originalParth, string newParth);

        /// <summary>
        /// 查找子栏目
        /// </summary>
        /// <param name="id">栏目Id</param>
        /// <returns></returns>
        IQueryable<Category> FindChildren(int id);

        /// <summary>
        /// 查找内容
        /// </summary>
        /// <param name="contentType">内容类型</param>
        /// <param name="incParent">包含父栏目</param>
        /// <returns></returns>
        IQueryable<Category> FindListByContentType(ContentType contentType, bool incParent = false);

        /// <summary>
        /// 查找栏目名称
        /// </summary>
        /// <param name="id">栏目Id</param>
        /// <returns></returns>
        Task<string> FindNameAsync(int id);
    }
}
