﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Ninesky.Models
{
    /// <summary>
    /// API接受到的消息
    /// </summary>
    public class ApiResponseMessage<T> where T :class
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Successed { get; set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// 简要原因
        /// </summary>
        public string ReasonPhrase { get; set; }

        /// <summary>
        /// JWT字符串
        /// </summary>
        public string Jwt { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public T Content { get; set; }
    }
}
