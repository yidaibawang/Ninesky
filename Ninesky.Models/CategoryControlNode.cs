﻿using System.Collections.Generic;

namespace Ninesky.Models
{
    /// <summary>
    /// 栏目控件节点
    /// </summary>
    public class CategoryControlNode
    {
        /// <summary>
        /// 标签
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 值【id】
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool Disabled { get; set; }

        public List<CategoryControlNode> Children { get; set; }

        public CategoryControlNode()
        {
            Children = new List<CategoryControlNode>();
        }
    }
}
