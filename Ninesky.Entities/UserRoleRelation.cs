﻿using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    /// <summary>
    /// 用户角色关系
    /// </summary>
    public class UserRoleRelation
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 用户I组Id
        /// </summary>
        [Required]
        public int RoleId { get; set; }
    }
}
