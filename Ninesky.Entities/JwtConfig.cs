﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ninesky.Entities
{
    public class JwtConfig
    {
        /// <summary>
        /// 算法【HS256】
        /// </summary>
        public string Algorithm { get; set; }

        /// <summary>
        /// 类型【JWT】
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 签发者
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// 过期时间【分钟】
        /// </summary>
        public int ExpMinute { get; set; }

        /// <summary>
        /// 秘钥
        /// </summary>
        public string Secret { get; set; }

    }
}
