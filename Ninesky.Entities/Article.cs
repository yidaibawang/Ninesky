﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    public class Article
    {
        [Key]
        public int ArticleId { get; set; }

        [Required]
        [Display(Name = "所属栏目")]
        public int CategoryId { get; set; }

        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Required]
        [Display(Name = "标题")]
        [StringLength(255)]
        public string Title { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [Display(Name = "作者")]
        [StringLength(255)]
        public string Author { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [Display(Name = "来源")]
        [StringLength(255)]
        public string Source { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "简介")]
        [StringLength(500)]
        public string Intro { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Display(Name = "内容")]
        public string Content { get; set; }

        /// <summary>
        /// 发布日期
        /// </summary>
        [Display(Name = "发布日日期")]
        [DataType(DataType.DateTime)]
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Display(Name = "状态")]
        public ArticleStatus Status { get; set; }

        public Article()
        {
            Status = ArticleStatus.Normal;
            UpdateTime = DateTime.Now;
        }

    }
    
    /// <summary>
    /// 文章状态
    /// </summary>
    public enum ArticleStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Display(Name = "草稿")]
        Draft,
        /// <summary>
        /// 退稿
        /// </summary>
        [Display(Name = "退稿")]
        SendBack,
        /// <summary>
        /// 正常
        /// </summary>
        [Display(Name = "正常")]
        Normal
    }
}
