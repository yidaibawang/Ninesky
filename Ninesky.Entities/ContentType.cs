﻿using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    /// <summary>
    /// 内容类型
    /// </summary>
    public enum ContentType
    {
        [Display(Name = "未设置")]
        NotSet,
        [Display(Name = "文章")]
        Article
    }
}
