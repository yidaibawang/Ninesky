﻿using Microsoft.EntityFrameworkCore;

namespace Ninesky.Entities
{
    public class NineskyDbContext:DbContext
    {
        public NineskyDbContext(DbContextOptions<NineskyDbContext> options):base(options) { }

        public DbSet<Admin> Admins { get; set; }

        /// <summary>
        /// 方法权限
        /// </summary>
        public DbSet<ActionPermissionRule> ActionPermissionRules { get; set; }
        /// <summary>
        /// 角色权限
        /// </summary>
        public DbSet<RolePermissionRule> RolePermissionRules { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
