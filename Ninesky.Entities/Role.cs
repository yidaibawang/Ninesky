﻿using System.ComponentModel.DataAnnotations;

namespace Ninesky.Entities
{
    /// <summary>
    /// 角色
    /// </summary>
    public class Role
    {
        [Key]
        public int RoleId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        [StringLength(500)]
        public string Description { get; set; }
    }
}
