﻿namespace Ninesky.Web.Models
{
    /// <summary>
    /// zTree节点
    /// </summary>
    public class zTreeNode
    {
        public int id { get; set; }

        public int pId { get; set; }

        public string name { get; set; }

        public string iconSkin { get; set; }

        public string target { get; set; }

        public string url { get; set; }

        public zTreeNode()
        {
            target = "_self";
        }
    }
}
