﻿var categoryType =
    [
        {
            value: 'General',
            label: '常规栏目'
        },
        {
            value: 'Page',
            label: '单页栏目'
        },
        {
            value: 'Link',
            label: '链接栏目'
        }
    ]