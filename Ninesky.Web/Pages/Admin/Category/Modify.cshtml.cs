﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Ninesky.Web.Pages.Admin.Category
{
    public class ModifyModel : PageModel
    {
        public int Id { get; set; }
        public void OnGetAsync(int id)
        {
            Id = id;
        }
    }
}