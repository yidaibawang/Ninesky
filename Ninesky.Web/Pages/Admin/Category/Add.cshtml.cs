﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Ninesky.Web.Pages.Admin.Category
{
    public class AddModel : PageModel
    {
        private ApiClient apiClient { get; set; }

        /// <summary>
        /// 父栏目id
        /// </summary>
        public int? parentId { get; set; }
        public IActionResult OnGetAsync(int? id)
        {
            parentId = id;
            return Page();
        }

        /// <summary>
        /// 栏目类型
        /// </summary>
        public AddModel(IConfiguration configuration)
        {
            var serverUrl = configuration.GetSection("ApiServer").GetValue<string>("url");
            apiClient = new ApiClient(serverUrl, string.Empty);
        }

    }
}